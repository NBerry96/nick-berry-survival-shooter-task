﻿using UnityEngine;

namespace CompleteProject
{
    public class GameOverManager : MonoBehaviour
    {
        public PlayerHealth playerHealth;       // Reference to the player's health.
		public Canvas mobileControls;			// Reference to the canvas holding the mobile controls

        Animator anim;                          // Reference to the animator component.


        void Awake ()
        {
            // Set up the reference.
            anim = GetComponent <Animator> ();
        }


        void Update ()
        {
            // If the player has run out of health...
            if(playerHealth.currentHealth <= 0)
            {
                // ... tell the animator the game is over.
                anim.SetTrigger ("GameOver");
#if MOBILE_INPUT
				// Hides the mobile controls
				mobileControls.enabled = false;
#endif
				// Disables this script so this doesn't repeatedly call
				this.enabled = false;
            }
        }
    }
}