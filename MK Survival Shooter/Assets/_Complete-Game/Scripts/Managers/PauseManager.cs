﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityStandardAssets.CrossPlatformInput;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PauseManager : MonoBehaviour {
	
	public AudioMixerSnapshot paused;
	public AudioMixerSnapshot unpaused;

	public RectTransform resumeButton;		// The Resume button in the pause menu
	public RectTransform quitButton;		// The quit button in the pause menu
	public int mobileResumeButtonWidth;		// The width that the Resume button will be on mobile (because the quit button is removed)

	public Canvas mobileControls;			// The canvas holding the mobile controls
	public Joystick mobileLeftJoystick;		// The left joystick
	public Joystick mobileRightJoystick;	// The right joystick
	
	Canvas canvas;
	
	void Start()
	{
		canvas = GetComponent<Canvas>();
#if MOBILE_INPUT
		// Disables the quit button in the pause menu and enlarges and centers the resume button
		quitButton.gameObject.SetActive(false);
		resumeButton.anchorMin = new Vector2(0.5f,0);
		resumeButton.anchorMax = new Vector2(0.5f,0);
		resumeButton.pivot = new Vector2(0.5f,0);
		resumeButton.anchoredPosition = new Vector2(0, resumeButton.anchoredPosition.y);
		resumeButton.sizeDelta = new Vector2(mobileResumeButtonWidth,resumeButton.sizeDelta.y);
#endif
	}
#if !MOBILE_INPUT
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			canvas.enabled = !canvas.enabled;
			Pause();
		}
	}
#endif

	// Function called if the pause button in the mobile UI is pressed. Disables the mobile UI
	public void MobilePause()
	{
		canvas.enabled = true;
		mobileLeftJoystick.ResetJoystick();
		mobileRightJoystick.ResetJoystick();
		mobileControls.enabled = false;
		Pause();
	}

	public void Pause()
	{
		Time.timeScale = Time.timeScale == 0 ? 1 : 0;
#if MOBILE_INPUT
		// If the game has just been unpaused, re-enable the mobile controls
		if (Time.timeScale == 1){
			mobileControls.enabled = true;
		}
#endif
		Lowpass ();
		
	}
	
	void Lowpass()
	{
		if (Time.timeScale == 0)
		{
			paused.TransitionTo(.01f);
		}
		
		else
			
		{
			unpaused.TransitionTo(.01f);
		}
	}
	
	public void Quit()
	{
#if UNITY_EDITOR
		EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}
}
